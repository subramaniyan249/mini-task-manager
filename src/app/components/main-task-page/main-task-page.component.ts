import { Component, OnInit } from '@angular/core';
import { TaskManagerServiceService } from '../../services/task-manager-service.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateTaskComponent } from '../create-task/create-task.component';
import { AssignUserComponent } from '../assign-user/assign-user.component';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-main-task-page',
  templateUrl: './main-task-page.component.html',
  styleUrls: ['./main-task-page.component.css'],
})
export class MainTaskPageComponent implements OnInit {
  users: Array<any> = [];
  tasks: Array<any> = [];
  loading: boolean = false;
  searchText: string;
  public dataSourceHigh: Array<any> = [];
  public filteredItems: Array<any> = [];
  public dataSourceMedium: Array<any> = [];
  public dataSourceLow: Array<any> = [];

  constructor(
    private taskManagerService: TaskManagerServiceService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.fetchUsers();
    this.fetchTasks();
  }

  assignCopy(){
    this.filteredItems = Object.assign([], this.dataSourceHigh);
 }

 //open create task component
  openDialog() {
    const dialogRef = this.dialog.open(CreateTaskComponent, {
      data: this.users,
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.fetchTasks();
      console.log(`Dialog result: ${result}`);
    });
  }

  //this will give all the users
  openUserDialog(task: any) {
    const dialogRef = this.dialog.open(AssignUserComponent, {
      data: { user: this.users, task: task },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.fetchTasks();
      console.log(`Dialog result: ${result}`);
    });
  }

  //get the list of users
  public fetchUsers() {
    this.taskManagerService.getAllUsers().subscribe(
      (results: any) => {
        this.users = results.users;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  //fetch all the tasks
  public fetchTasks() {
    this.loading = true;
    this.taskManagerService.getAllTasks().subscribe(
      (results: any) => {
        this.loading = false;
        this.tasks = results.tasks;
        let group = this.tasks.reduce((r, a) => {
          r[a.priority] = [...(r[a.priority] || []), a];
          return r;
        }, {});

        this.dataSourceHigh = group[1];
        this.dataSourceMedium = group[2];
        this.dataSourceLow = group[3];
      },
      (error: any) => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  drop(event: CdkDragDrop<any>, priority: string) {
    let requestBody = event.previousContainer.data[event.previousIndex];
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.loading = true;
      requestBody['priority'] = priority;
      requestBody['taskid'] = requestBody['id'];
      var form_data = new FormData();

      for (var key in requestBody) {
        form_data.append(key, requestBody[key]);
      }
      this.taskManagerService.updateTask(form_data).subscribe(
        (results: any) => {
          this.loading = false;
          this.fetchTasks();
          console.log(results);
        },
        (error: any) => {
          this.loading = false;
          console.log(error);
        }
      );
    }
  }

  deleteTask(id: string) {
    this.loading = true;
    var form_data = new FormData();
    form_data.append('taskid', id);
    this.taskManagerService.deleteTask(form_data).subscribe(
      (results: any) => {
        this.loading = false;
        this.fetchTasks();
        console.log(results);
      },
      (error: any) => {
        this.loading = false;
        console.log(error);
      }
    );
  }
}
