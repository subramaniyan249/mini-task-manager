import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskManagerServiceService } from '../../services/task-manager-service.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';



@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
})
export class CreateTaskComponent implements OnInit {
  createTaskForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private taskManagerService: TaskManagerServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.createTaskForm = this.fb.group({
      message: ['', Validators.required],
      due_date: [''],
      priority: [''],
      assigned_to: [''],
    });
  }

  //create task
  public createTask(): void {
    var formData = new FormData();
    Object.keys(this.createTaskForm.controls).forEach((key) => {
      let value = this.createTaskForm.controls[key].value;
      formData.append(key, value);
    });
    //formatting date time
    if (formData.getAll('due_date')[0]) {
      let value =
        formData.getAll('due_date').toString().replace('T', ' ') + ':00';
      formData.set('due_date', value);
    }

    this.taskManagerService.createTask(formData).subscribe(
      (results: any) => {
        console.log(results);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}
