import { Component, OnInit, Inject } from '@angular/core';
import { TaskManagerServiceService } from '../../services/task-manager-service.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-assign-user',
  templateUrl: './assign-user.component.html',
  styleUrls: ['./assign-user.component.css']
})
export class AssignUserComponent implements OnInit {
  users : Array<any> = []
  changedUser: string;
  constructor(
    private taskManagerService: TaskManagerServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.users = this.data.user
  }

  assignUser(event: any) {
    this.changedUser = event.value;
  }

  //change user and assign the task
  changeUser() {
    let requestBody = this.data.task;
    requestBody['assigned_to'] = this.changedUser;
    requestBody['taskid'] = requestBody['id'];
    var form_data = new FormData();

      for (var key in requestBody) {
        form_data.append(key, requestBody[key]);
      }
      this.taskManagerService.updateTask(form_data).subscribe(
        (results: any) => {
          console.log(results);
        },
        (error: any) => {
          console.log(error);
        }
      );
  }

}
