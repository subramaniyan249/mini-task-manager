import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HeaderInterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = 'iwptbUu7Ylzmy0SHZ2Q9U2v3q2QnjCVV';
    const request = req.clone({ 
      headers: req.headers.set('AuthToken', `${userToken}`),
    });
    return next.handle(request);
  }
}
