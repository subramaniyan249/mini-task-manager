import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainTaskPageComponent } from './components/main-task-page/main-task-page.component';

const routes: Routes = [
  {
    path: 'tasks',
    component: MainTaskPageComponent,
  },
  {
    path: '',
    component: MainTaskPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
