import { Injectable } from '@angular/core';
import {
  HttpClient,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TaskManagerServiceService {
  baseURL: string = "https://devza.com/tests/tasks";
  constructor(private http: HttpClient) {}

  public getAllUsers() {

    return this.http.get<any>(`${this.baseURL}/listusers`);
  }

  public getAllTasks() {
    return this.http.get<any>(`${this.baseURL}/list`);
  }

  public createTask(formData: any) {
    return this.http.post<any>(`${this.baseURL}/create`, formData);
  }

  public updateTask(formData: any) {
    return this.http.post<any>(`${this.baseURL}/update`, formData);
  }

  public deleteTask(formData: any) {
    return this.http.post<any>(`${this.baseURL}/delete`, formData);
  }

}
